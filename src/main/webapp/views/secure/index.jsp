<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@page session="true"%>
<%@ page contentType="text/html; charset=UTF-8"%>

<html ng-app="app">

<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">

<title>Dashboard</title>
<!-- CSS -->
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/resources/css/bootstrap.min.css" />
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/resources/css/custom.css" />
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/resources/css/font-awesome.min.css" />
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/resources/css/export.css" />
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/resources/css/rzslider.css" />
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/resources/css/angular-notify.min.css" />

<link rel="stylesheet"
	href="${pageContext.request.contextPath}/resources/css/dashboard.css" />

<link rel="stylesheet"
	href="${pageContext.request.contextPath}/resources/css/comentarios.css" />

<link rel="stylesheet"
	href="${pageContext.request.contextPath}/resources/css/jqcloud.css" />


<link rel="stylesheet"
	href="${pageContext.request.contextPath}/resources/css/datepicker.css" />
<link rel="stylesheet" type="text/css"
	href="https://cdn.jsdelivr.net/angular.chartjs/0.8.8/angular-chart.min.css" />

<!-- leeflet -->
<link rel="stylesheet"
	href="http://cdn.leafletjs.com/leaflet/v0.7.7/leaflet.css" />
<link rel="stylesheet" type="text/css"
	href="http://cdnjs.cloudflare.com/ajax/libs/leaflet.markercluster/0.4.0/MarkerCluster.css" />
<link rel="stylesheet" type="text/css"
	href="http://cdnjs.cloudflare.com/ajax/libs/leaflet.markercluster/0.4.0/MarkerCluster.Default.css" />


<!-- cluster de puntos -->
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/resources/css/MarkerCluster.css" />
<!--  muestra fotos en mapa -->
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/resources/css/Leaflet.Photo.css" />

<!--  -->

</head>

<body class="nav-md" ng-controller="homeController" ng-init="init();">

	<div class="container body">
		<div class="main_container">
			<!-- left navigation -->
			<div class="col-md-3 left_col" set-height>

				<div class="left_col scroll-view">

					<div class="navbar nav_title" style="border: 0;">

						<span> <img
							src="${pageContext.request.contextPath}/resources/img/pueblos-logo.png"
							style="margin-bottom: 20px; margin-top: 20px; height: 72px; width: 72px; margin-left: auto; display: block; margin-right: auto;"
							class="img-responsive" id="imgLogo">
						</span> <span class="hide text-center" id="logoText">SECTUR</span>


					</div>
					<div class="clearfix"></div>


					<br /> <br />
					<!-- sidebar menu -->
					<div id="sidebar-menu"
						class="main_menu_side hidden-print main_menu">

						<div class="menu_section" style="margin-top: 15px">
							<h3></h3>
							<ul class="nav side-menu">

								<li><a ui-sref="inicio"><i class="fa fa-home fa-lg"></i>
										Inicio</a></li>
								<li><a ui-sref="indicadores"><i
										class="fa fa-tachometer fa-lg"></i> Indicadores</a></li>
								<li><a><i class="fa fa-map-marker fa-lg"></i>
										Localidades</a>

									<ul class="nav child_menu">
										<li><a ui-sref="localidades"> Indicadores </a></li>
										<li><a ui-sref="servicios"> Servicios </a></li>
										<li><a ui-sref="visitantes"> Visitantes </a></li>
										<li><a ui-sref="comentarios"> Comentarios </a></li>



									</ul></li>

							</ul>
						</div>
					</div>

					<div class="profile" style="bottom: -10px; position: relative;">
						<div class="profile_pic">
							<img
								src="${pageContext.request.contextPath}/resources/img/user.png"
								alt="..." class="img-circle profile_img">
						</div>
						<div class="profile_info">
							<span>Bienvenido,</span>
							<h2>${pageContext.request.userPrincipal.name}</h2>
							<a href="/sectur/logout" style="margin-top: 20px; color: #efefef">
								<i class="fa fa-sign-out pull-right"></i> Cerrar sesión
							</a>
						</div>
					</div>
					<!-- /sidebar menu -->
				</div>
			</div>
			<!-- /left navigation -->

			<!-- top navigation -->
			<div class="top_nav">

				<div class="nav_menu">
					<nav class="" role="navigation">
						<div class="nav toggle">
							<a id="menu_toggle"><i class="fa fa-bars"></i></a>
						</div>

						<ul class="nav navbar-nav navbar-right">


							<li><a href="javascript:;"
								class="user-profile dropdown-toggle" data-toggle="dropdown"
								aria-expanded="false"
								style="margin-top: 15px; margin-right: 10px"><i
									class="fa  fa-chevron-left fa-2x" aria-hidden="true"
									style="padding-right: 2px;"></i><i class="fa  fa-cogs fa-2x"
									aria-hidden="true" style="padding-right: 2px;"></i> </a>
								<ul
									class="dropdown-menu dropdown-usermenu animated fadeInDown pull-right">
									<li><a ui-sref="configuracion">Configuración</a></li>
									<li><a ui-sref="usuarios">Usuarios</a></li>
									<li><a ui-sref="aplicacion">Aplicación</a></li>
									<li><a href="${pageContext.request.contextPath}/logout"><i
											class="fa fa-sign-out pull-right"></i> Cerrar sesión</a></li>
								</ul></li>

							<li><img
								src="${pageContext.request.contextPath}/resources/img/sectur_logo.png"
								class="img-responsive" id="imgLogo" style="margin-right: 20px;"></li>
						</ul>
					</nav>
				</div>

			</div>
			<!-- /top navigation -->

			<!-- page content -->
			<div class="right_col" role="main">
				<div ui-view="view" class="row"></div>
			</div>
			<!-- /page content -->
		</div>
	</div>

	<!-- scrips para necesarios -->
	<script
		src="${pageContext.request.contextPath}/resources/js/plugins/jquery/jquery.js"></script>
	<script
		src="${pageContext.request.contextPath}/resources/js/plugins//bootstrap/bootstrap.min.js"></script>
	<script
		src="${pageContext.request.contextPath}/resources/js/plugins/angularjs/angular.min.js"></script>

	<script
		src="${pageContext.request.contextPath}/resources/js/plugins/locale/angular-locale_es-mx.js"></script>

	<script
		src="${pageContext.request.contextPath}/resources/js/plugins/angularjs/angular-touch.min.js"></script>
	<script
		src="${pageContext.request.contextPath}/resources/js/plugins/angularjs/angular-route.min.js"></script>
	<script
		src="${pageContext.request.contextPath}/resources/js/plugins/angularjs/angular-animate.min.js"></script>
	<script
		src="${pageContext.request.contextPath}/resources/js/plugins/ui-router/angular-ui-router.js"></script>
	<script
		src="${pageContext.request.contextPath}/resources/js/plugins/bootstrap/ui-bootstrap-tpls.js"></script>
	<script
		src="${pageContext.request.contextPath}/resources/js/plugins/amcharts/amcharts.js"></script>
	<script
		src="${pageContext.request.contextPath}/resources/js/plugins/amcharts/pie.js"></script>
	<script
		src="${pageContext.request.contextPath}/resources/js/plugins/amcharts/radar.js"></script>
	<script
		src="${pageContext.request.contextPath}/resources/js/plugins/amcharts/serial.js"></script>
	<script
		src="${pageContext.request.contextPath}/resources/js/plugins/amcharts/plugins/export/export.min.js"></script>
	<script
		src="${pageContext.request.contextPath}/resources/js/plugins/amcharts/plugins/responsive/responsive.min.js"></script>
	<script
		src="${pageContext.request.contextPath}/resources/js/plugins/rzslider/rzslider.js"></script>
	<script
		src="${pageContext.request.contextPath}/resources/js/plugins/spin/spin.js"></script>
	<script
		src="${pageContext.request.contextPath}/resources/js/plugins/datepicker/bootstrap-datepicker.js"></script>

	<script
		src="${pageContext.request.contextPath}/resources/js/plugins/angular-notify/angular-notify.min.js"></script>

	<script
		src="${pageContext.request.contextPath}/resources/js/plugins/angular-notify/angular-notify.min.js"></script>

	<script
		src="${pageContext.request.contextPath}/resources/js/plugins/ammap/ammap.js"></script>
	<script
		src="${pageContext.request.contextPath}/resources/js/plugins/ammap/ammap.js"></script>
	<script
		src="${pageContext.request.contextPath}/resources/js/plugins/ammap/themes/light.js"></script>
	<script
		src="${pageContext.request.contextPath}/resources/js/plugins/ammap/maps/js/worldLow.js"></script>
	<script
		src="${pageContext.request.contextPath}/resources/js/plugins/ammap/maps/js/worldHigh.js"></script>
	<script
		src="${pageContext.request.contextPath}/resources/js/plugins/ammap/maps/js/mexicoHigh.js"></script>
	<script
		src="${pageContext.request.contextPath}/resources/js/plugins/ammap/lang/es.js"></script>

	<script
		src="${pageContext.request.contextPath}/resources/js/plugins/jQCloud/jqcloud.js"></script>

	<!-- angular-chart  -->
	<script
		src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/1.0.2/Chart.min.js"></script>

	<script
		src="https://cdn.jsdelivr.net/angular.chartjs/0.8.8/angular-chart.min.js"></script>

	<script
		src="${pageContext.request.contextPath}/resources/js/app/config/app-config.js"></script>

	<script
		src="${pageContext.request.contextPath}/resources/js/app/config.js"></script>

	<script
		src="${pageContext.request.contextPath}/resources/js/app/directive/datepicker-directive.js"></script>

	<script
		src="${pageContext.request.contextPath}/resources/js/app/factories/app-factory.js"></script>


	<script
		src="${pageContext.request.contextPath}/resources/js/app/controller/dashboard-controller.js"></script>

	<script
		src="${pageContext.request.contextPath}/resources/js/app/controller/index-controller.js"></script>

	<script src="http://cdn.leafletjs.com/leaflet/v0.7.7/leaflet.js"></script>

	<script
		src="${pageContext.request.contextPath}/resources/js/plugins/datepicker/bootstrap-datepicker.js"></script>
	<script
		src="${pageContext.request.contextPath}/resources/js/app/controller/localidad-controller.js"></script>
	<script
		src="${pageContext.request.contextPath}/resources/js/app/controller/indicador-controller.js"></script>

	<script
		src="${pageContext.request.contextPath}/resources/js/app/controller/usuario-controller.js"></script>

	<script
		src="${pageContext.request.contextPath}/resources/js/app/controller/servicios-controller.js"></script>

	<script
		src="${pageContext.request.contextPath}/resources/js/app/controller/visitantes-controller.js"></script>

	<script
		src="${pageContext.request.contextPath}/resources/js/app/controller/comentarios-controller.js"></script>

	<script
		src="${pageContext.request.contextPath}/resources/js/app/controller/configuracion-controller.js"></script>
<script
		src="${pageContext.request.contextPath}/resources/js/app/controller/aplicacion-controller.js"></script>

	<!-- leaflet scripts -->

	<script
		src="${pageContext.request.contextPath}/resources/js/plugins/reqwest/reqwest.min.js"></script>

	<script src="http://cdn.leafletjs.com/leaflet/v0.7.7/leaflet.js"></script>

	<script type='text/javascript'
		src='http://cdnjs.cloudflare.com/ajax/libs/leaflet.markercluster/0.4.0/leaflet.markercluster.js'></script>

	<script
		src="${pageContext.request.contextPath}/resources/js/plugins/leaflet-fullscreen/Control.FullScreen.js"></script>


	<script
		src="${pageContext.request.contextPath}/resources/js/plugins/leaflet-cluster/leaflet.markercluster.js"></script>

	<script
		src="${pageContext.request.contextPath}/resources/js/plugins/photo-pages/Leaflet.Photo.js"></script>

	<!--  -->

	<script
		src="${pageContext.request.contextPath}/resources/js/plugins/datepicker/bootstrap-datepicker.js"></script>

</body>
</html>
