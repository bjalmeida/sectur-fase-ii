<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@page session="true"%>
<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">

<title>Login</title>

<!-- CSS -->
<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/bootstrap.min.css" />
<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/font-awesome.min.css" />
<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/login.css" />

<!-- scrips para login -->
<script src="${pageContext.request.contextPath}/resources/js/plugins/jquery/jquery.js" ></script>
<script src="${pageContext.request.contextPath}/resources/js/plugins/bootstrap/bootstrap.min.js" ></script>
<script src="${pageContext.request.contextPath}/resources/js/plugins/getURLParam/getURLParam.js" ></script>
<script type="text/javascript">
	$(document).ready(function() {
		$('#error').hide();
		var error = $.getURLParam("error");
		if (error == null) {
			$('#error').hide();
		} else {
			$('#error').show();
		}
	});
</script>
</head>
<body style="background: #F7F7F7;">
	<div class="">
		<a class="hiddenanchor" id="toregister"></a> <a class="hiddenanchor"
			id="tologin"></a>

		<div id="wrapper">
			<div id="login" class=" form">
				<section class="login_content">
				<form action="j_spring_security_check" method="post">
					<img src="${pageContext.request.contextPath}/resources/img/logo.png" class="img-responsive" style="margin-left: auto; display: block; margin-right: auto;">
					<br>
					<div>
						<input type="text" class="form-control" placeholder="Nombre de usuario"
							required="" id="j_username" name="j_username" />
					</div>
					<br>
					<div>
						<input type="password" class="form-control" placeholder="Contraseña"
							required="" id="j_password" name="j_password" />
					</div>
					<br>
					<div class="alert alert-danger col-md-12" role="alert" id="error">
						<span class="fa fa-info-circle"
							aria-hidden="true"></span> <span class="sr-only">Error:</span>
						Credenciales no válidas
					</div>
					<br>
					<div>
						<input type="submit" class="btn btn-default submit button" value="Iniciar sesión" />
					</div>	
				</form>
				</section>
			</div>
		</div>
	</div>
</body>
</html>