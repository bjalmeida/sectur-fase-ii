app.directive('datepicker', function() {
	return function(scope, element, attrs) {    	
		element.datepicker({
			inline: false,
			restrict: 'EAC',
			require: 'ngModel',

			onSelect: function(dateText) {
				var modelPath = $(this).attr('ng-model');
				putObject(modelPath, scope, dateText);
				scope.$apply();                
			}
		});
	};
});

app.directive('setHeight', function($window){
	return{
		link: function(scope, element, attrs){
			element.css('height', $window.innerHeight + 'px');
			//element.height($window.innerHeight/3);
		}
	}
});

app.directive('setHeightComments', function($window){
	return{
		link: function(scope, element, attrs){
			element.css('height', ($window.innerHeight/2+50)  + 'px');
			//element.height($window.innerHeight/3);
		}
	}
});
