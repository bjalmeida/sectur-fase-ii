app.factory('JsonFactory', ['$http', function($http){

	var dataFactory ={};

	dataFactory.getPricesByTypeAndDate = function(params){
		return $http({
			method: 'GET',
			url: '/cdmxturismo/rest/api/1.0/getPricesByTypeAndDate',
			data : params,
			params : params
		});
	};

	dataFactory.getTopTenHoteles = function(params){
		return $http({
			method: 'GET',
			url: '/cdmxturismo/rest/api/1.0/getTopTenHotelesByTypeAndDate',
			data : params,
			params : params
		});
	};


	dataFactory.getPricesByTypeAndDateAndRank = function(params){
		return $http({
			method: 'GET',
			url: '/cdmxturismo/rest/api/1.0/getPricesByTypeAndDateAndRank',
			data : params,
			params : params
		});
	};

	dataFactory.getTopTenRestaurantsPerDateAndCategory = function(params){
		return $http({
			method: 'GET',
			url: '/cdmxturismo/rest/api/1.0/getTopTenRestaurantsPerDateAndCategory',
			data : params,
			params : params
		});
	};

	dataFactory.getHistoricoPreciosByTypeAndDate = function(params){
		return $http({
			method: 'GET',
			url: '/cdmxturismo/rest/api/1.0/getHistoricoPreciosByTypeAndDate',
			data : params,
			params : params
		});
	};

	dataFactory.getTypeTraveler = function(params){
		return $http({
			method: 'GET',
			url: '/cdmxturismo/rest/api/1.0/getTypeTraveler',
			data : params,
			params: params

		});
	};

	dataFactory.getTypeTravelerPromedio = function(params){
		return $http ({
			method:'GET',
			url: '/cdmxturismo/rest/api/1.0/getTypeTravelerPromedio',
			data:params,
			params:params
		});
	};


	
	dataFactory.getTravelerNationalityPerDay = function(params){
		return $http({
			method: 'GET',
			url: '/cdmxturismo/rest/api/1.0/getTravelerNationalityPerDay',
			data : params,
			params : params
		});
	};
	
	dataFactory.getTravelerNationalityPerRankDate = function(params){
		return $http({
			method: 'GET',
			url: '/cdmxturismo/rest/api/1.0/getTravelerNationalityPerRankDate',
			data : params,
			params : params
		});
	};
	
	dataFactory.getCountGender = function(params){
		return $http({
			method: 'GET',
			url: '/cdmxturismo/rest/api/1.0/getCountGender',
			data : params,
			params : params
		});
	};
	
	dataFactory.getCountGenderRank = function(params){
		return $http({
			method: 'GET',
			url: '/cdmxturismo/rest/api/1.0/getCountGenderRank',
			data : params,
			params : params
		});
	};
	
	dataFactory.getCharactersByDay = function(params){
		return $http({
			method: 'GET',
			url: '/cdmxturismo/rest/api/1.0/getCharactersByDay',
			data : params,
			params : params
		});
	};
	
	dataFactory.getInfoByDay = function(params){
		return $http({
			method: 'GET',
			url: '/cdmxturismo/rest/api/1.0/getInfoByDay',
			data : params,
			params : params
		})
	};
	
	dataFactory.getFlickrFotos = function(){
		return $http({
			method: 'GET',
			url: '/cdmxturismo/rest/api/1.0/getFlickrFotos',
//			data : params,
//			params : params
		});
	};
	
	dataFactory.getFlickrFotosFecha = function(params){
		return $http({
			method: 'GET',
			url: '/cdmxturismo/rest/api/1.0/getFlickrFotosFecha',
			data : params,
			params : params
		});
	};
	
	dataFactory.getCharactersRank = function(params){
		return $http({
			method: 'GET',
			url: '/cdmxturismo/rest/api/1.0/getCharactersRank',
			data : params,
			params : params
		});
	};
	
	dataFactory.getInfoRank = function(params){
		return $http({
			method: 'GET',
			url: '/cdmxturismo/rest/api/1.0/getInfoRank',
			data : params,
			params : params
		});
	};
	
	dataFactory.getCountAge = function(params){
		return $http({
			method: 'GET',
			url: '/cdmxturismo/rest/api/1.0/getCountAge',
			data : params,
			params : params
		});
	};
	
	dataFactory.getCountAgeRank = function(params){
		return $http({
			method: 'GET',
			url: '/cdmxturismo/rest/api/1.0/getCountAgeRank',
			data : params,
			params : params
		});
	};
	
	dataFactory.getCountExtranjerosPerDay = function(params){
		return $http({
			method: 'GET',
			url: '/cdmxturismo/rest/api/1.0/getCountExtranjerosPerDay',
			data : params,
			params : params
		});
	};
	
	dataFactory.getCountExtranjerosPerRank = function(params){
		return $http({
			method: 'GET',
			url: '/cdmxturismo/rest/api/1.0/getCountExtranjerosPerRank',
			data : params,
			params : params
		});
	};
	
	dataFactory.getTimeOfYearByDay = function(params){
		return $http({
			method: 'GET',
			url: '/cdmxturismo/rest/api/1.0/getTimeOfYearByDay',
			data : params,
			params : params
		});
	};
	
	dataFactory.getTimeOfYearByRank = function(params){
		return $http({
			method: 'GET',
			url: '/cdmxturismo/rest/api/1.0/getTimeOfYearByRank',
			data : params,
			params : params
		});
	};
	
	dataFactory.getFavoriteFoodPerDay = function(params){
		return $http({
			method: 'GET',
			url: '/cdmxturismo/rest/api/1.0/getFavoriteFoodPerDay',
			data : params,
			params : params
		});
	};
	
	dataFactory.getFavoriteFoodPerRank = function(params){
		return $http({
			method: 'GET',
			url: '/cdmxturismo/rest/api/1.0/getFavoriteFoodPerRank',
			data : params,
			params : params
		});
	};
	
	dataFactory.getAverageDays = function(params){
		return $http({
			method: 'GET',
			url: '/cdmxturismo/rest/api/1.0/getAverageDaysPerDay',
			data: params,
			params: params
		});
	};
	
	dataFactory.getVisitedAtractions = function(params){
		return $http({
			method: 'GET',
			url: '/cdmxturismo/rest/api/1.0/getVisitedAtractions',
			data: params,
			params: params
		});
	};
	
	dataFactory.getVisitedRestaurants = function(params){
		return $http({
			method: 'GET',
			url: '/cdmxturismo/rest/api/1.0/getVisitedRestaurants',
			data: params,
			params: params
		});
	};
	
	dataFactory.getCountCommentsPerDay = function(params){
		return $http({
			method: 'GET',
			url: '/cdmxturismo/rest/api/1.0/getCountCommentsPerDay',
			data: params,
			params: params
		});
	}
	
	dataFactory.getCountCommentsPerRank = function(params){
		return $http({
			method: 'GET',
			url: '/cdmxturismo/rest/api/1.0/getCountCommentsPerRank',
			data: params,
			params: params
		});
	}
	
	dataFactory.getCountCommentsAirbnbPerDay = function(params){
		return $http({
			method: 'GET',
			url: '/cdmxturismo/rest/api/1.0/getCountCommentsAirbnbPerDay',
			data: params,
			params: params
		});
	}
	
	dataFactory.getCountCommentsAirbnbPerRank = function(params){
		return $http({
			method: 'GET',
			url: '/cdmxturismo/rest/api/1.0/getCountCommentsAirbnbPerRank',
			data: params,
			params: params
		});
	}
	
	dataFactory.getCommentsFilter = function(params){
		return $http({
			method: 'GET',
			url: '/cdmxturismo/rest/api/1.0/getCommentsFilter',
			data: params,
			params: params
		});
	}
	
	dataFactory.getCommentById = function(params){
		return $http({
			method: 'GET',
			url: '/cdmxturismo/rest/api/1.0/getCommentById',
			data: params,
			params: params
		});
	}
	
	dataFactory.getCountComments = function(params){
		return $http({
			method: 'GET',
			url: '/cdmxturismo/rest/api/1.0/getCountComments',
			data: params,
			params: params
		});
	}
	
	dataFactory.setHasComment = function(params){
		return $http({
			method: 'POST',
			url: '/cdmxturismo/rest/api/1.0/setHasComment',
			data: params,
			params: params
		});
	}
	
	dataFactory.changeSentiment = function(params){
		return $http({
			method: 'POST',
			url: '/cdmxturismo/rest/api/1.0/changeSentiment',
			data: params,
			params: params
		});
	}
	
	dataFactory.getGeneralRestaurants = function(params){
		return $http({
			method: 'GET',
			url: '/cdmxturismo/rest/api/1.0/getGeneralRestaurants',
			data: params,
			params: params
		});
	}
	
	dataFactory.getGeneralHotels = function(params){
		return $http({
			method: 'GET',
			url: '/cdmxturismo/rest/api/1.0/getGeneralHotels',
			data: params,
			params: params
		});
	}
	
	dataFactory.getHistoryHotelesValues = function(params){
		return $http({
			method: 'GET',
			url: '/cdmxturismo/rest/api/1.0/getHistoryHotelesValues',
			data: params,
			params: params
		});
	}
	
	dataFactory.getHistoryRestValues = function(params){
		return $http({
			method: 'GET',
			url: '/cdmxturismo/rest/api/1.0/getHistoryRestValues',
			data: params,
			params: params
		});
	}
	
	dataFactory.getCountSentiment = function(){
		return $http({
			method: 'GET',
			url: '/cdmxturismo/rest/api/1.0/getCountSentiment',
		});
	}
	
	dataFactory.getHistoryRestValues = function(params){
		return $http({
			method: 'GET',
			url: '/cdmxturismo/rest/api/1.0/getHistoryRestValues',
			data: params,
			params: params
		});
	}
	
	dataFactory.getCommentTwitterOrFacebook = function(params){
		return $http({
			method: 'GET',
			url: '/cdmxturismo/rest/api/1.0/getCommentTwitterOrFacebook',
			data: params,
			params: params
		});
	}
	
	dataFactory.getTweetOrPostById = function(params){
		return $http({
			method: 'GET',
			url: '/cdmxturismo/rest/api/1.0/getTweetOrPostById',
			data: params,
			params: params
		});
	}
	
	dataFactory.getAverageDaysRank = function(params){
		return $http({
			method: 'GET',
			url: '/cdmxturismo/rest/api/1.0/getAverageDaysPerRankDay',
			data: params,
			params: params
		});
	}
	
	dataFactory.countComments = {};
	
	dataFactory.comments = [];
	
	return dataFactory;
}]);


