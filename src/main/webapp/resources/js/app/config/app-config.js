var app = angular.module('app', ['ui.router','ngAnimate','rzModule', 'ui.bootstrap','chart.js', 'cgNotify']);



app.config(function($stateProvider,$urlRouterProvider, $compileProvider){


	$urlRouterProvider.otherwise("/inicio");


	$stateProvider
	.state('inicio', {
		url:"/inicio",
		views: {
			"view" : {
				templateUrl: 'views/secure/templates/inicio.html'
			}
		}
	})
	.state('indicadores', {
		url:"/indicadores",
		views: {
			"view" : {
				templateUrl: 'views/secure/templates/indicadores.html'
			}
		}
	})
	.state('localidades', {
		url:"/localidades",
		views: {
			"view" : {
				templateUrl: 'views/secure/templates/localidades_indicadores.html'
			}
		}
	})
	
	
	.state('servicios', {
		url:"/servicios",
		views: {
			"view" : {
				templateUrl: 'views/secure/templates/servicios.html'
			}
		}
	})
	
	.state('visitantes', {
		url:"/visitantes",
		views: {
			"view" : {
				templateUrl: 'views/secure/templates/visitantes.html'
			}
		}
	})
	
	.state('comentarios', {
		url:"/comentarios",
		views: {
			"view" : {
				templateUrl: 'views/secure/templates/comentarios.html'
			}
		}
	})
	
	
	.state('aplicacion', {
		url:"/aplicacion",
		views: {
			"view" : {
				templateUrl: 'views/secure/templates/aplicacion.html'
			}
		}
	})
	.state('usuarios', {
		url:"/usuarios",
		views: {
			"view" : {
				templateUrl: 'views/secure/templates/usuarios.html'
			}
		}
	})
	.state('configuracion', {
		url:"/configuracion",
		views: {
			"view" : {
				templateUrl: 'views/secure/templates/configuracion.html'
			}
		}
	})
	;
	
	// configure new 'compile' directive by passing a directive
    // factory function. The factory function injects the '$compile'
    $compileProvider.directive('compile', function($compile) {
      // directive factory creates a link function
      return function(scope, element, attrs) {
        scope.$watch(
          function(scope) {
             // watch the 'compile' expression for changes
            return scope.$eval(attrs.compile);
          },
          function(value) {
            // when the 'compile' expression changes
            // assign it into the current DOM
            element.html(value);

            // compile the new DOM and link it to the current
            // scope.
            // NOTE: we only compile .childNodes so that
            // we don't get into infinite loop compiling ourselves
            $compile(element.contents())(scope);
          }
        );
      };
    });

	
});