app.controller('comentariosController', ['$scope', '$filter', 'JsonFactory', 'notify', '$window', function($scope, $filter, JsonFactory, notify, $window) {

	var opts = {
        lines: 13 // The number of lines to draw
            ,
        length: 15 // The length of each line
            ,
        width: 15 // The line thickness
            ,
        radius: 41 // The radius of the inner circle
            ,
        scale: 0.25 // Scales overall size of the spinner
            ,
        corners: 1 // Corner roundness (0..1)
            ,
        color: '#000' // #rgb or #rrggbb or array of colors
            ,
        opacity: 0.25 // Opacity of the lines
            ,
        rotate: 0 // The rotation offset
            ,
        direction: 1 // 1: clockwise, -1: counterclockwise
            ,
        speed: 1 // Rounds per second
            ,
        trail: 60 // Afterglow percentage
            ,
        fps: 20 // Frames per second when using setTimeout() as a fallback for CSS
            ,
        zIndex: 2e9 // The z-index (defaults to 2000000000)
            ,
        className: 'spinner' // The CSS class to assign to the spinner
            ,
        top: '50%' // Top position relative to parent
            ,
        left: '50%' // Left position relative to parent
            ,
        shadow: false // Whether to render a shadow
            ,
        hwaccel: false // Whether to use hardware acceleration
            ,
        position: 'absolute' // Element positioning
    }

	 	
	
    var target = document.getElementById('spin');
    var spinner = new Spinner(opts);

    $scope.comentarios = ['un comentario:Lorem Ipsum es simplemente el texto de relleno de las imprentas y archivos de texto. Lorem Ipsum ha sido el texto de relleno estándar de las industrias desde el año 1500, cuando un impresor (N. del T. persona que se dedica a la imprenta) desconocido usó una galería de textos y los mezcló de tal manera que logró hacer un libro de textos especimen. No sólo sobrevivió 500 años, sino que tambien ingresó como texto de relleno en documentos electrónicos, ','otro comentario','3 comentario:Lorem Ipsum es simplemente el texto de relleno de las imprentas y archivos de texto. Lorem Ipsum ha sido el texto de relleno estándar de las industrias desde el año 1500, cuando un impresor (N. del T. persona que se dedica a la imprenta) desconocido usó una galería de textos y los mezcló de tal manera que logró hacer un libro de textos especimen. No sólo sobrevivió 500 años, sino que tambien ingresó como texto de relleno en documentos electrónicos, ']
    $scope.fullComentario='';
  
    $scope.showComentario = function(val){
    	
    	  $scope.fullComentario=val;
    }
    
    $scope.muestraError = function(mensaje, clase, template, position, duration) {
        notify({
            message: mensaje,
            classes: 'alert-danger',
            templateUrl: template,
            position: position,
            duration: duration
        });
        spinner.stop();
    };


}]);