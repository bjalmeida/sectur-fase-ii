app.controller('indicadorController', ['$scope', '$filter', 'JsonFactory', 'notify', '$window', function($scope, $filter, JsonFactory, notify, $window) {

	var opts = {
        lines: 13 // The number of lines to draw
            ,
        length: 15 // The length of each line
            ,
        width: 15 // The line thickness
            ,
        radius: 41 // The radius of the inner circle
            ,
        scale: 0.25 // Scales overall size of the spinner
            ,
        corners: 1 // Corner roundness (0..1)
            ,
        color: '#000' // #rgb or #rrggbb or array of colors
            ,
        opacity: 0.25 // Opacity of the lines
            ,
        rotate: 0 // The rotation offset
            ,
        direction: 1 // 1: clockwise, -1: counterclockwise
            ,
        speed: 1 // Rounds per second
            ,
        trail: 60 // Afterglow percentage
            ,
        fps: 20 // Frames per second when using setTimeout() as a fallback for CSS
            ,
        zIndex: 2e9 // The z-index (defaults to 2000000000)
            ,
        className: 'spinner' // The CSS class to assign to the spinner
            ,
        top: '50%' // Top position relative to parent
            ,
        left: '50%' // Left position relative to parent
            ,
        shadow: false // Whether to render a shadow
            ,
        hwaccel: false // Whether to use hardware acceleration
            ,
        position: 'absolute' // Element positioning
    }


	
	 	
    var target = document.getElementById('spin');
    var spinner = new Spinner(opts);

    var dateInicial = new Date();
//	esconde o muestra la grafica de historial
    $scope.showHistorial=false;
    $scope.ngClass="7";
    
    $scope.minDate = new Date(2016, 5, 29);
    $scope.maxDate = new Date();

    $scope.datepickers = {
        dt: false,
        dtSecond: false
    }
    $scope.today = function() {
        $scope.dateIncial = new Date();
        $scope.dateFinal = new Date();
    };

    $scope.showWeeks = true;
    $scope.toggleWeeks = function() {
        $scope.showWeeks = !$scope.showWeeks;
    };

    $scope.clear = function() {
        $scope.dt = null;
    };

    $scope.open = function($event, which) {
        $event.preventDefault();
        $event.stopPropagation();

        $scope.datepickers[which] = true;
    };

    $scope.dateOptions = {
        'year-format': "'yy'",
        'starting-day': 1
    };

    $scope.formats = ['yyyy-MM-dd', 'dd-MMMM-yyyy', 'yyyy/MM/dd', 'shortDate'];
    $scope.format = $scope.formats[0];
  
    $scope.init = function() {
    	$scope.dateInicial = "2016-06-29";
    	$scope.dateFinal = new Date();
    	spinner.spin(target);
        var params = {};
        params.start = $scope.dateInicial;
        params.end = $scope.dateFinal.toISOString();

        $scope.hgt = $window.innerHeight / 1.289;

        
        $scope.inicio = $scope.dateInicial;
        $scope.fin = $scope.dateFinal;

    };



    $scope.filtrarFechas = function() {
    	 var arr = [];

        spinner.spin(target);
       
        spinner.stop();
    };

 
	
	 $scope.labelsLine = ["January", "February", "March", "April", "May", "June", "July"];
   $scope.seriesLine = ['Series A', 'Series B'];
   $scope.dataLine = [
     [65, 59, 80, 81, 56, 55, 40],
     [28, 48, 40, 19, 86, 27, 90]
   ];
   
   
   $scope.toggle = function(){
	   $scope.showHistorial = !$scope.showHistorial;
	   
	   
	   if($scope.showHistorial==true)
		   $scope.ngClass = "9";
	   		else
	   		 $scope.ngClass = "7";
	   
   }
   
   $scope.changeColor = function(val) {


	      if (val > 70 && val <= 100) { //verde
	         return 'verdeBg';
	      } else if (val >= 50 && val <= 70) { //naranja
	         return 'naranjaBg';
	      } else if (val >= 0 && val < 50) { //rojo
	         return 'rojoBg';
	      } else { // sin datos gris
	         return 'grisBg';
	      }
	   }

    	
 $scope.pueblos = [
                   {
                	nombre:'pueblo',
                	estado:'mexico',
                	tipo:'pueblo magico'                	
                   }
                  ,{
                	  nombre:'puebla',
                  	estado:'lugar n',
                  	tipo:'destino turistico' 
                  }
                  ,{
                	  nombre:'colima',
                  	estado:'este',
                  	tipo:'ciudad magica' 
                  }];
    

    $scope.muestraError = function(mensaje, clase, template, position, duration) {
        notify({
            message: mensaje,
            classes: 'alert-danger',
            templateUrl: template,
            position: position,
            duration: duration
        });
        spinner.stop();
    };


}]);