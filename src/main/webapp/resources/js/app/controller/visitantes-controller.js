app.controller('visitantesController', ['$scope', '$filter', 'JsonFactory', 'notify', '$window', function($scope, $filter, JsonFactory, notify, $window) {

	var opts = {
        lines: 13 // The number of lines to draw
            ,
        length: 15 // The length of each line
            ,
        width: 15 // The line thickness
            ,
        radius: 41 // The radius of the inner circle
            ,
        scale: 0.25 // Scales overall size of the spinner
            ,
        corners: 1 // Corner roundness (0..1)
            ,
        color: '#000' // #rgb or #rrggbb or array of colors
            ,
        opacity: 0.25 // Opacity of the lines
            ,
        rotate: 0 // The rotation offset
            ,
        direction: 1 // 1: clockwise, -1: counterclockwise
            ,
        speed: 1 // Rounds per second
            ,
        trail: 60 // Afterglow percentage
            ,
        fps: 20 // Frames per second when using setTimeout() as a fallback for CSS
            ,
        zIndex: 2e9 // The z-index (defaults to 2000000000)
            ,
        className: 'spinner' // The CSS class to assign to the spinner
            ,
        top: '50%' // Top position relative to parent
            ,
        left: '50%' // Left position relative to parent
            ,
        shadow: false // Whether to render a shadow
            ,
        hwaccel: false // Whether to use hardware acceleration
            ,
        position: 'absolute' // Element positioning
    }

	 	
	$scope.labelsEdad = ["<18", "18-25", "26-35","36-50","50+"];
	  $scope.dataEdad = [700, 500, 400,800,888];
	  $scope.coloursEdad =['#FF0000','#FFFF33','#FF9900','#00CC99','#00FF33'];
	
		$scope.labelsGenero = ["Hombres", "Mujeres"];
		  $scope.dataGenero= [570,888];
		  $scope.coloursGenero =['#FF0000','#00FF33'];
	
    var target = document.getElementById('spin');
    var spinner = new Spinner(opts);

    $scope.chart_options = {
    	    multiTooltipTemplate: function(label) {
    	        return label.value + '%';
    	    }
    	}
    
   $scope.init = function(){
    	
    	var words = [
    	             {text: "Lorem", weight: 13},
    	             {text: "Ipsum", weight: 10.5},
    	             {text: "Dolor", weight: 9.4},
    	             {text: "Sit", weight: 8},
    	             {text: "Amet", weight: 6.2},
    	             {text: "Consectetur", weight: 5},
    	             {text: "Adipiscing", weight: 5},
    	            
    	           ];
    	$scope.createCloudWords(words);
    }
    
    $scope.createCloudWords = function(data){
		$('#cloudWord').jQCloud(data);
	};

    $scope.muestraError = function(mensaje, clase, template, position, duration) {
        notify({
            message: mensaje,
            classes: 'alert-danger',
            templateUrl: template,
            position: position,
            duration: duration
        });
        spinner.stop();
    };


}]);