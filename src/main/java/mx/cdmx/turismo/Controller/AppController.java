package mx.cdmx.turismo.Controller;

import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class AppController {
	
	@RequestMapping(value="/index", method = RequestMethod.GET)
	public String index(ModelMap modelMap){
		return "secure/index";
	}
	
	@RequestMapping(value="/login", method = RequestMethod.GET)
	public String login(){
		return "unsecure/login";
	}
	
	@RequestMapping(value ="/logout", method = RequestMethod.GET)
	public String logout(ModelMap model, HttpSession session) {
		return "redirect:/j_spring_security_logout";
	}
	
	@RequestMapping(value = "/", method = RequestMethod.GET)
	public String index() {
		return "redirect:/index";
	}
	
}
